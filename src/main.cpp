//============================================================================
// Name        : rozpoznavani.cpp
// Author      : milecold
// Version     :
// Copyright   : milecold@fit.cvut.cz
// Description : MI-ROZ tamura features classifier
//============================================================================
#include <string>
#include <iostream>
#include "lodepng.h"
#include <vector>
#include <fstream>
#include <algorithm>
#include <climits>
#include <map>
#include <set>
#include <cmath>
#include <queue>
#include <list>
#include <cfloat>
#include <fstream>
using namespace std;

class Image {
    //image class for image representation and feature extraction
public:

    Image(string filename) {
        //loads Image from a PNG image
        unsigned error = lodepng::decode(image, width, height,
                filename.c_str());
        if (error) {
            std::cout << "decoder error " << error << ": "
                    << lodepng_error_text(error) << std::endl;
            cout << filename << endl;
        }
        //save class id
        subject = 0;
        subject += (filename[7] - '0') * 100;
        subject += (filename[8] - '0') * 10;
        subject += (filename[9] - '0') * 1;
    }

    Image(vector<vector<unsigned char> > values) {
    //get image from a vector of values, used for regularity subimages
        height = values.size();
        width = values.at(0).size();
        cropImage = values;
    }

    Image(unsigned subject, vector<double> features ) {
    //get Image only using class and features from file
        this->subject = subject;
        this->features = features;
    }

    void convertGrey() {
    //convert to greyscale and discard alpha
        unsigned char r, g, b;
        grey.reserve(width * height);
        for (unsigned i = 0; i < image.size(); i += 4) {
            r = image.at(i);
            g = image.at(i + 1);
            b = image.at(i + 2);
            grey.push_back(0.30 * r + 0.59 * g + 0.11 * b);
        }
    }

    void cropImages() {
    //crop image and store it into a 2D matrix
        cropImage.reserve(80);
        for (int i = 10; i < height - 10; i++) {
            vector<unsigned char> tmp(grey.begin() + i * width + 150, grey.begin()+(i + 1) * width - 150);
            cropImage.push_back(tmp);
        }
        height = cropImage.size();
        width = cropImage.at(0).size();
    }

    void calculateFeatures(bool regular) {
    //calculate all  6 features
        features.push_back(getCoarsness());
        features.push_back(getContrast());
        features.push_back(getDirectionality());
        features.push_back(getLinelikeness());
        if (regular) {
            features.push_back(getRoughness());
            features.push_back(getRegularity());
        }
    }

    const vector<unsigned char> getGrey() const {
        return grey;
    }

    const vector<double> getFeatures() const {
        return features;
    }

    const unsigned getClass() const {
        return subject;
    }

private:

    double averageValue(int area, int h, int w) {
        //average vallue of a neighborhood of area size around point h, w, edge is mirrored
        int sum = 0;
        int count = 0;
        int i1, j1;
        for (int i = h - area; i <= h + area; i++) {
            for (int j = w - area; j <= w + area; j++) {
                if (i < 0) {
                    i1 = abs(i);
                } else if (i >= height) {
                    i1 = height - (i % height) - 1;
                } else {
                    i1 = i;
                }
                if (j < 0) {
                    j1 = abs(j);
                } else if (j >= width) {
                    j1 = width - (j % width) - 1;
                } else {
                    j1 = j;
                }
                sum += (int) cropImage.at(i1).at(j1);
                count++;
            }
        }
        return (double) sum / count;
    }

    double getCoarsness() {
        //calculate coarseness feature
        vector<vector<vector<double> > > averages;
        //fill with zeros and allocate size
        averages.resize(6);
        for (int i = 0; i < 6; i++) {
            averages[i].resize(height);
            for (int j = 0; j < height; j++) {
                averages[i][j].resize(width);
            }
        }
        vector<vector<vector<double> > > horizontal = averages;
        vector<vector<vector<double> > > vertical = averages;
        vector<vector<double> > best;
        best.resize(height);
        for (int i = 0; i < height; i++) {
            best.at(i).resize(width);
        }

        //calculate average grey level in area around pixel, area size from 2^0 to 2^5
        for (int k = 0; k < 6; k++) {
            int area = pow(2, k);
            for (int h = 0; h < height; h++) {
                for (int w = 0; w < width; w++) {
                    averages[k][h][w] = averageValue(area, h, w);
                }
            }
            for (int h = area + 1; h < height - area - 1; h++) {
                for (int w = area + 1; w < width - area - 1; w++) {
                    vertical[k][h][w] = abs(averages[k][h + area + 1][w] - averages[k][h - area - 1][w]);
                    horizontal[k][h][w] = abs(averages[k][h][w + area + 1] - averages[k][h][w - area - 1]);
                    //cout << horizontal[k][h][w] << " " << vertical[k][h][w] << endl;
                }
            }
        }

        //get the maximum difference for each pixel
        double maxH, maxV;
        int maxHindex, maxVindex, index;

        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                maxH = 0;
                maxV = 0;
                maxVindex = 0;
                maxHindex = 0;
                for (int k = 0; k < 6; k++) {
                    if (horizontal[k][h][w] > maxH) {
                        maxH = horizontal[k][h][w];
                        maxHindex = k;
                    }
                    if (vertical[k][h][w] > maxV) {
                        maxV = vertical[k][h][w];
                        maxVindex = k;
                    }
                }
                index = maxH > maxV ? maxHindex : maxVindex;
                best[h][w] = pow(2, index);
            }
        }
        //get the average best value
        double sum = 0;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                sum += best[h][w];
            }
        }
        //cout << "coarsness: " << sum / (height * width) << endl;
        return (sum / (height * width));
    }

    double getContrast() {
        //calculate contrast feature
        double mean = 0, var = 0, cmoment4 = 0, kurtosis, contrast;
        int count = width * height;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                mean += cropImage[h][w];
            }
        }
        //mean of grey values
        mean /= count;
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                cmoment4 += pow((cropImage[h][w] - mean), 4);
                var += pow((cropImage[h][w] - mean), 2);
            }
        }
        //variance
        var /= count;
        cmoment4 /= count;
        kurtosis = cmoment4 / (var * var);
        contrast = pow(var, 0.5) / pow(kurtosis, 0.25);

        //cout << "contrast: " << contrast << endl;
        return contrast;
    }

    double getDirectionality() {
        //calculate directionality
        //2 empty vectors for edges
        vector<vector<double> > verticalEdge, horizontalEdge;
        verticalEdge.resize(height);
        for (int i = 0; i < height; i++) {
            verticalEdge.at(i).resize(width);
        }
        horizontalEdge = verticalEdge;
        magnitudeVector.reserve(width * height);
        thetaVector.reserve(width * height);

        int h1, w1, h2, w2;
        //Prewitt convolution
        for (int h = 0; h < height; h++) {
            //mirror edge cases
            if (h == 0) {
                h1 = 1;
                h2 = h + 1;
            } else if (h == height - 1) {
                h2 = height - 2;
                h1 = h - 1;
            } else {
                h1 = h - 1;
                h2 = h + 1;
            }
            for (int w = 0; w < width; w++) {
                //mirror edge cases
                if (w == 0) {
                    w1 = 1;
                    w2 = w + 1;
                } else if (w == width - 1) {
                    w2 = width - 2;
                    w1 = w - 1;
                } else {
                    w1 = w - 1;
                    w2 = w + 1;
                }

                //calculate convolution for horizontal and vertical edge detection
                horizontalEdge[h][w] += -1 * cropImage[h1][w1];
                horizontalEdge[h][w] += 1 * cropImage[h1][w2];
                horizontalEdge[h][w] += -1 * cropImage[h][w1];
                horizontalEdge[h][w] += 1 * cropImage[h][w2];
                horizontalEdge[h][w] += -1 * cropImage[h2][w1];
                horizontalEdge[h][w] += 1 * cropImage[h2][w2];

                verticalEdge[h][w] += 1 * cropImage[h1][w1];
                verticalEdge[h][w] += 1 * cropImage[h1][w];
                verticalEdge[h][w] += 1 * cropImage[h1][w2];
                verticalEdge[h][w] += -1 * cropImage[h2][w1];
                verticalEdge[h][w] += -1 * cropImage[h2][w];
                verticalEdge[h][w] += -1 * cropImage[h2][w2];
                //get the magnitude of the edges
                magnitudeVector.push_back(abs(verticalEdge[h][w]) + abs(horizontalEdge[h][w]) / 2);
                //get the edge direction
                if (verticalEdge[h][w] == 0 && horizontalEdge[h][w] == 0) {
                    thetaVector.push_back(0);
                } else if (horizontalEdge[h][w] == 0) {
                    thetaVector.push_back(M_PI);
                } else {
                    thetaVector.push_back(atan(verticalEdge[h][w] / horizontalEdge[h][w]) + M_PI / 2);
                }
            }
        }

        //heuristic constants
        //quantize edges with magnitude higher than a threshold t
        int n = 6, threshold = 16;
        double partitions[n] = {0};
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < magnitudeVector.size(); j++) {
                if (magnitudeVector[j] > threshold && thetaVector[j] > (2 * i - 1)*(M_PI / (2 * n)) && thetaVector[j] < (2 * i + 1)*(M_PI / (2 * n))) {
                    partitions[i]++;
                }
            }
        }
        queue<int> peaks;
        queue<int> valleys;
        double previous = 0;
        double mean = 0;
        for (int i = 0; i < n; i++) {
            mean += partitions[i];
        }
        bool valley = false;
        mean /= n;

        //normalize and get peaks and valleys
        for (int i = 0; i < n; i++) {
            if (mean != 0) {
                partitions[i] /= mean;
            }
            if (partitions[i] > previous) {
                if (valley) {
                    valleys.push(i - 1);
                }
                valley = false;
                if (i == n - 1) {
                    peaks.push(i);
                }

            } else if (partitions[i] < previous) {
                if (!valley && i != 0) {
                    peaks.push(i - 1);
                }
                valley = true;
                if (i == n - 1) {
                    valleys.push(i);
                }
            }
            previous = partitions[i];
        }
        //if the histogram is completely uniform
        if (peaks.size() == 0) {
            peaks.push(0);
        }

        //get directionality as 1 number
        //calculates second moments for each peak
        double directionality = 0;
        for (int i = 0; i < n; i++) {
            //get to the next peak
            if (valleys.size() && i == valleys.front()) {
                valleys.pop();
                //do not remove the last peak
                if (peaks.size() > 1) {
                    peaks.pop();
                }
            }
            //get the second moment
            directionality += pow((i - peaks.front()), 2) * partitions[i];
        }

        //cout << "directionality: " << directionality << endl;
        return directionality;
    }

    double getRoughness() {
        //rougness is defined as coarseness + contrast
        double roughness = features.at(0) + features.at(1);

        //cout << "roughness: " << roughness << endl;
        return roughness;
    }

    double getLinelikeness() {
        //9 directions from 0 to PI
        int coocurrence[9][9] = {0};
        int h1, w1, h_scaled, w_scaled;
        //get coocurrence matrix of edges
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                if (magnitudeVector[h * width + w] > 16) {
                    h1 = h - sin(thetaVector[h * width + w]);
                    w1 = w + cos(thetaVector[h * width + w]);
                    if (h1 < 0 || w1 < 0) {
                        h1 = abs(h1);
                        w1 = abs(w1);
                    }
                    if (h1 >= height) {
                        h1 = height - (h % height) - 2;
                    }
                    if (w1 >= width) {
                        w1 = width - (w % width) - 2;
                    }
                    if (magnitudeVector[h1 * width + w1] > 16) {
                        h_scaled = ceil(thetaVector[h * width + w]*8 / M_PI);
                        w_scaled = ceil(thetaVector[h1 * width + w1]*8 / M_PI);
                        coocurrence[h_scaled][w_scaled]++;
                    }
                }
            }
        }
        //get one value from coocurrence matrix, same direction is weighed +1 perpendicular -1
        double top = 0, bottom = 0, linelikeness;
        for (int h = 0; h < 9; h++) {
            for (int w = 0; w < 9; w++) {
                top += coocurrence[h][w] * cos(abs((h * M_PI) / 8 - (w * M_PI) / 8)*(2 * M_PI) / 9);
                bottom += coocurrence[h][w];
            }
        }
        if (bottom == 0) {
            linelikeness = 0;
        } else {
            linelikeness = top / bottom;
        }
       //cout << "linelikeness: " << linelikeness << endl;
        return linelikeness;
    }

    double getRegularity() {
        //split image into 12 non overlapping subimages
        vector<vector<vector<unsigned char> > > subimages;
        subimages.resize(12);
        for (int h = 0; h < height; h++) {
            for (int j = 0; j < 6; j++) {
                vector<unsigned char> tmp(cropImage[h].begin() + j * width / 6, cropImage[h].begin()+(j + 1) * width / 6);
                subimages[(h / 40)*6 + j].push_back(tmp);
            }
        }
        double coarse[12];
        double line[12];
        double direct[12];
        double contr[12];
        double meanCoarse = 0, meanLine = 0, meanDirect = 0, meanContr = 0;
        double sigmaCoarse = 0, sigmaLine = 0, sigmaDirect = 0, sigmaContr = 0;

        //get features for those subimages
        for (int i = 0; i < 12; i++) {

            Image tmp(subimages[i]);
            coarse[i] = tmp.getCoarsness();
            meanCoarse += coarse[i];
            direct[i] = tmp.getDirectionality();
            meanDirect += direct[i];
            line[i] = tmp.getLinelikeness();
            meanLine += line[i];
            contr[i] = tmp.getContrast();
            meanContr += contr[i];
        }
        //calculate mean and deviation for each feature
        meanCoarse /= 12;
        meanContr /= 12;
        meanDirect /= 12;
        meanLine /= 12;
        for (int i = 0; i < 12; i++) {
            sigmaCoarse += pow(coarse[i] - meanCoarse, 2);
            sigmaLine += pow(line[i] - meanLine, 2);
            sigmaDirect += pow(direct[i] - meanDirect, 2);
            sigmaContr += pow(contr[i] - meanContr, 2);
        }
        sigmaCoarse = pow(sigmaCoarse / 11, 0.5);
        sigmaLine = pow(sigmaLine / 11, 0.5);
        sigmaDirect = pow(sigmaDirect / 11, 0.5);
        sigmaContr = pow(sigmaContr / 11, 0.5);

        //sum deviation
        double regularity = sigmaCoarse + sigmaLine + sigmaDirect + sigmaContr;
        //cout <<"regularity: "<< regularity << endl;
        return regularity;

    }

    unsigned width;
    unsigned height;
    unsigned subject;
    vector<vector<unsigned char> > cropImage;
    vector<double> features;
    vector<unsigned char> image;
    vector<unsigned char> grey;
    vector<double> magnitudeVector, thetaVector;
};

class DataSet {
public:

    DataSet(const char *filename, float ratio) {
        //load all images from a file
        vector<double> features;
        for (int i = 0; i < 6; i++) {
            maxFeatures.push_back(0);
            minFeatures.push_back(DBL_MAX);
        }
        string image_name;
        ifstream in(filename);
        while (getline(in, image_name)) {
            image_name = "images/" + image_name;
            training.push_back(Image(image_name.c_str()));
        }
        ofstream output;
        //save features into this file
        output.open("features.txt");

        //load all images convert them crop them and get their features
        for (unsigned i = 0; i < training.size(); i++) {
            training.at(i).convertGrey();
            training.at(i).cropImages();
            training.at(i).calculateFeatures(true);
            features = training.at(i).getFeatures();
            output << training.at(i).getClass() << " ";
            //check if new features are new highest or lowest
            for (int j = 0; j < features.size(); j++) {
                output << features.at(j)<<" ";
                cout << features.at(j)<<" ";
                if (features.at(j) > maxFeatures.at(j)) {
                    maxFeatures.at(j) = features.at(j);
                }
                if (features.at(j) < minFeatures.at(j)) {
                    minFeatures.at(j) = features.at(j);
                }
            }
            cout << endl;
            output << endl;
        }

        //split vector into two testing and training sets
        split(ratio);
    }

    DataSet(const char *filename){
        //load images only based on precomputed features and classes
        ifstream in;
        in.open(filename);
        int index;
        int i = 0;
        double feature;
        vector<double> loaded;
        for(int i = 0;i<6;i++){
            minFeatures.push_back(DBL_MAX);
            maxFeatures.push_back(0);
        }
        while(in >> index){
            loaded.resize(0);
            i++;
            for(int i = 0; i < 6; i++){
                in >> feature;
                loaded.push_back(feature);
            }
            if(loaded.at(5) < 8 && loaded.at(0) < 10 && loaded.at(1) > 4){
                training.push_back(Image(index, loaded));
                cout << index << endl;
            }
        }

        vector<double> features;
        for(unsigned i = 0; i < training.size(); i++) {

            features = training.at(i).getFeatures();
            for(int j = 0; j < features.size(); j++) {
                if (features.at(j) > maxFeatures.at(j)) {
                    maxFeatures.at(j) = features.at(j);
                }
                if (features.at(j) < minFeatures.at(j)) {
                    minFeatures.at(j) = features.at(j);
                }
            }
        }

        split(0.3);
    }

    void split(float ratio){
        //split images into a training and testing sets
        unsigned current,split,size;
        bool even = false;
        vector<Image> tmp;
        vector<Image> sameClass;
        while(training.size()){
            //make sure that trainng set contains all classes evenly
            current = training.back().getClass();
            while(training.size() && training.back().getClass() == current){
                sameClass.push_back(training.back());
                training.pop_back();
            }
            cout << sameClass.size() << endl;
            if(sameClass.size() > 1){
                split = ratio * sameClass.size();
                random_shuffle(sameClass.begin(), sameClass.end());
                if(sameClass.size()%2){
                    even = !even;
                    if(even){
                        split++;
                    }  
                }
            
                for(unsigned j = 0; j < split; j++) {
                    testing.push_back(sameClass.back());
                    sameClass.pop_back();
                }
                size = sameClass.size();
                for(unsigned j = 0; j < size; j++){
                    tmp.push_back(sameClass.back());
                    sameClass.pop_back();
                }
            }else{
                sameClass.pop_back();
            }
            
        }
        training = tmp;
        cout << training.size() << " vs " << testing.size() << endl;
    }

    float classify(int k) {
        unsigned best_class;
        unsigned maxC;
        double current_distance;
        unsigned correct = 0;
        vector<pair<double, unsigned> > best;

        //compare each image features to all from the trainig set
        for(unsigned i = 0; i < testing.size(); i++) {
            maxC = 0;
            best.resize(0);
            best_class = 0;
            map<unsigned,unsigned> counts;
            for(int j = 0; j< k;j++){
                best.push_back({DBL_MAX,0});
            }
            
            //get k closest distances
            for(unsigned j = 0; j < training.size(); j++) {
                current_distance = distance(testing.at(i).getFeatures(), training.at(j).getFeatures());
                for(int l = 0; l < k; l++){
                    if(current_distance < best.at(l).first){
                        //cout << "new best: "<<current_distance<<" "<<training.at(j).getClass()<<endl;

                        best.insert(best.begin() + l, {current_distance, j});
                        best.pop_back();
                        break;
                    }
                }
            }
            
            //increment neighbors based on classes
            for(int j = 0; j < k; j++){
                //cout << "tyto sousede: "<<training.at(best.at(j).second).getClass() << endl;
                auto it = counts.find(training.at(best.at(j).second).getClass());
                if(it != counts.end()){
                    it->second++;
                }else{
                    counts.insert({training.at(best.at(j).second).getClass(), 1});
                }
            }
            //find the most represented neighbor, in case of ties choose the closest one
            for(auto it : counts){
                if(it.second > maxC){
                    //cout <<"this neighbour: "<< it.second <<" " <<it.first<<  endl;
                    best_class = it.first;
                    maxC = it.second;
                }else if(it.second == maxC){
                    for(int j = 0; j < k; j++){
                        if(training.at(best.at(j).second).getClass() == best_class){
                            break;
                        }else if(training.at(best.at(j).second).getClass() == it.first){
                            best_class = it.first;
                            break;
                        }
                    }
                }
            }

            //check if the chosen class is correct
            if (testing.at(i).getClass() == best_class) {
                cout << "OK! Třída: " << testing.at(i).getClass() << endl;
                correct++;
            } else {
                cout << " Chyba! Třída: " << best_class << " místo: " << testing.at(i).getClass()  << endl;
            }
        }
        cout << "Správně: " << correct << " z " << testing.size() << endl;
        cout << "Úspěšnost " << ((float) correct / testing.size()) * 100 << "%" << endl;
        return ((float) correct / testing.size()) * 100;
    }


private:

    double distance(vector<double> a, vector<double> b) {
        //L1 distance
        double c, d;
        double x = 0;

        //normalize features
        for (int i = 0; i < a.size(); i++) {
            if(maxFeatures.at(i) - minFeatures.at(i) != 0){
                a.at(i) = ((a.at(i) - minFeatures.at(i)) / (maxFeatures.at(i) - minFeatures.at(i)));
                b.at(i) = ((b.at(i) - minFeatures.at(i)) / (maxFeatures.at(i) - minFeatures.at(i)));
            }
        }

        for (unsigned i = 0; i < a.size(); i++) {
            c = a.at(i);
            d = b.at(i);
            x += abs(c - d);
        }
   
        return x;
    }

    double vecLength(vector<double> a, vector<double> b) {
        //L2 distance
        double x = 0;
        for (unsigned i = 0; i < a.size(); i++) {
            x += (a.at(i) - b.at(i)) * (a.at(i) - b.at(i));
        }
        return sqrt(x);
    }
    vector<double> minFeatures;
    vector<double> maxFeatures;
    vector<Image> training;
    vector<Image> testing;
};

int main() {
    srand(time(0));
    DataSet a("images/images.txt", 0.5);
    a.classify(5);
}
